from django.urls import path
from accounts import views
from accounts.views import signup



urlpatterns = [
    path('login/', views.login_view, name='login'),
    path('logout/', views.logout_view, name='logout'),
    path('signup/', signup, name='signup'),
]
