from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account, Receipt
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm
# Create your views here.

@login_required(login_url='/accounts/login/')
def receipt_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    return render(request, 'receipts/list.html', {'receipts': receipts})

@login_required
def create_receipt(request):
    if request.method == 'POST':
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect('home')
    else:
        form = ReceiptForm()

    return render(request, 'receipts/create.html', {'form': form})


@login_required
def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    return render(request, 'categories/list.html', {'categories': categories})


@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    return render(request, 'accounts/list.html', {'accounts': accounts})


@login_required
def create_expense_category(request):
    if request.method == 'POST':
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            expense_category = form.save(commit=False)
            expense_category.owner = request.user
            expense_category.save()
            return redirect('category_list')
    else:
        form = ExpenseCategoryForm()

    return render(request, 'categories/create.html', {'form': form})


@login_required
def create_account(request):
    if request.method == 'POST':
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(commit=False)
            account.owner = request.user
            account.save()
            return redirect('account_list')
    else:
        form = AccountForm()

    return render(request, 'accounts/create.html', {'form': form})
